﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour
{


    public static CoinManager initialization;

    private Transform player;
    public GameObject coinPrefabs;

    private void Awake()
    {
               
        initialization = GetComponent<CoinManager>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Start()
    {

    }

    internal GameObject CreatCoin()
    {

        GameObject tempCoin = Instantiate(coinPrefabs, transform);

        tempCoin.transform.position = new Vector3(player.position.x + (Random.Range(-15, 15)), 1, player.position.z + (Random.Range(0, 75)));

        return tempCoin;
    }



}
