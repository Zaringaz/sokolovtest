﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Time" , menuName = "Time")]
public class FieldTimer : ScriptableObject
{
    public float time;


}
