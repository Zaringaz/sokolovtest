﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))] // Компонент будет точно присоединен


public class PlayerController : MonoBehaviour // Наследуемся
{

    private Animator animatorPlayer;
    private float speedPlayer;

    public Joystick joystick;
    

    private void Awake()
    {
        animatorPlayer = GetComponent<Animator>();
    }

    void Start()
    {

    }


    void FixedUpdate()
    {

        Vector3 tempPositionPlayer = Vector3.zero;

        if (joystick.Horizontal != 0 && joystick.Vertical != 0) // произошло двидение стика
        {
            tempPositionPlayer.x = joystick.Horizontal;
            tempPositionPlayer.z = joystick.Vertical;


            Quaternion direction = Quaternion.LookRotation(tempPositionPlayer);

            transform.rotation = Quaternion.Lerp(transform.rotation, direction, 1);

            animatorPlayer.SetBool("Walk", true);

            if (Mathf.Abs(joystick.Horizontal) >= 0.5f || Mathf.Abs(joystick.Vertical) >= 0.5f)
            {
                speedPlayer += 0.02f;
            }
            else
            {
                speedPlayer -= 0.02f;
            }

            animatorPlayer.SetFloat("Speed", speedPlayer);
        }
        else
        {
            speedPlayer = 0;
            animatorPlayer.SetFloat("Speed", speedPlayer);
            animatorPlayer.SetBool("Walk", false);
        }



    }


    public void JumpPlayer()
    {
        bool Ground = false;
        RaycastHit hitGround;

        if (Physics.Raycast(transform.position, Vector3.down, out hitGround))
        {
            float chekDistantion = 0.01f;

            if (hitGround.distance <= chekDistantion)
            {
                Ground = true;
            }
            else
            {
                Ground = false;
            }

        }

        if (Ground)
        {
            animatorPlayer.SetTrigger("Jump");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            GameController.upCoin();
            Destroy(other.gameObject);
        }

        if(other.tag == "Point")
        {
            UIController.initialization.WinnerPanel();
            Destroy(other.gameObject);


        }
    }

}
