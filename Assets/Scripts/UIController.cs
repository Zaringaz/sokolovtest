﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController initialization;

    public Text countCoin;
    public Text timerDeath;

    public List<GameObject> panelGame;

    public FieldTimer timer;


    private void Awake()
    {
        initialization = GetComponent<UIController>();
    }

    void Start()
    {
        timer.time = 60;
        timerDeath.text = "Time: " + timer.time.ToString();
        StartCoroutine(TimerDeath());
    }



    public void SetCoinCount(int coin)
    {
        countCoin.text = "CountCoin: " + coin.ToString();
    }


    private IEnumerator TimerDeath()
    {
        yield return new WaitForSeconds(1.0f);

        timer.time--;
        timerDeath.text = "Time: " + timer.time;

        if (timer.time <= 0)
        {
            Time.timeScale = 0;


            panelGame[0].SetActive(false);
            panelGame[1].SetActive(true);

            panelGame[1].GetComponentInChildren<Text>().text = "Coin: " + GameController.initialization.coin.ToString();

        }
        else
        {
            StartCoroutine(TimerDeath());
        }

    }

    public void WinnerPanel ()
    {
        StopAllCoroutines();
        panelGame[0].SetActive(false);
        panelGame[2].SetActive(true);
        panelGame[2].GetComponentInChildren<Text>().text = "Coin: " + GameController.initialization.coin.ToString();
        Time.timeScale = 0;
    }

    private void OnDestroy()
    {
       
    }


}
