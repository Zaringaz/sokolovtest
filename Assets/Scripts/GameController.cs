﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class GameController : MonoBehaviour
{
    public static GameController initialization;

    public Transform playerPosition;

    public static System.Action upCoin;


    public int countCoinCreat;

    public  int coin;



    private void Awake()
    {
        upCoin += UPCoin;
        Time.timeScale = 1;

        initialization = GetComponent<GameController>();

    }


    void Start()
    {



        UIController.initialization.SetCoinCount(coin);

        for (int i = 0; i < countCoinCreat; i++)
        {
            CoinManager.initialization.CreatCoin();
        }
    }






    private void UPCoin()
    {
        coin++;
        UIController.initialization.SetCoinCount(coin);
    }

    public void RestartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
        
    }


    private void OnDestroy()
    {
        upCoin -= UPCoin;
    }


}
